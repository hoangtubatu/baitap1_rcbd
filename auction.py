from flask import Flask
from sqlalchemy import *
import numpy as np
metadata = MetaData()
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import mapper
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from datetime import datetime
app = Flask(__name__)
try:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://apple:123456/@localhost/postgresDemo'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    print("Success")
except:
    print("Fail")

db = SQLAlchemy(app)
# migration database
migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

# bảng trung gian


class Bid(db.Model):
    __tablename__ = 'bid'
    id = db.Column(db.Integer(), primary_key=True)
    price = db.Column(db.Float, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'))
    user = db.relationship('User', backref='user_associate')
    item = db.relationship('Item', backref='item_associate')


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(60), nullable=False)
    password = db.Column(db.String(60), nullable=False)
    # many to many with item, dùng backref để trỏ đến items
    items = db.relationship('Item', secondary='bid',
                            backref=db.backref('items', lazy='dynamic'))


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False)
    description = db.Column(db.String(60), nullable=False)
    start_time = db.Column(db.DateTime, default=datetime.utcnow)
    users = db.relationship('User', secondary='bid',
                            backref=db.backref('users', lazy='dynamic'))


@app.route('/item/<int:item_id>')
def user_best_bid(item_id):
    try:
        bids = Bid.query.filter_by(item_id=item_id)
        np.sort(bids, 'order'=price)
        return json.dumps(bids[-1].user)
    except Exception as error:
        return str(error)


if __name__ == "__main__":
    manager.run()
    app.run()
